#ifndef __egor9814__parcel__parcel_hpp__
#define __egor9814__parcel__parcel_hpp__

#include <cstdint>
#include <vector>
#include <string>
#include <list>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <array>

namespace parcel {
	/// forward declarations
	using byte_t = uint8_t;
	using byte_array_t = std::vector<byte_t>;
	class Parcel;
	

	/**
	 * @brief Parcelable interface
	 * */
	struct Parcelable {
		virtual void writeToParcel(Parcel& out) const = 0;
		virtual void readFromParcel(const Parcel& in) = 0;
	};


	/**
	 * @brief Parcel reader/writer
	 * */
	class Parcel {
	    struct bytes_t {
            byte_array_t mBytes;
            mutable size_t mOffset {0};

            bytes_t() = default;
            explicit bytes_t(byte_array_t bytes) : mBytes(std::move(bytes)) {}

            bytes_t(const bytes_t&) = delete;
            bytes_t(bytes_t&&) noexcept = delete;
            bytes_t&operator=(const bytes_t&) = delete;
            bytes_t&operator=(bytes_t&&) noexcept = delete;

            void read(byte_t* ptr, size_t size) const {
                auto a = available();
                auto s = size > a ? a : size;
                auto data = mBytes.data() + mOffset;
                for (size_t i = 0; i < s; i++, ptr++, data++) {
                    *ptr = *data;
                }
                mOffset += s;
            }

            void write(const byte_t* ptr, size_t size) {
                mBytes.reserve(size);
                for (size_t i = 0; i < size; i++) {
                    mBytes.push_back(ptr[i]);
                }
            }

            size_t available() const {
                return mBytes.size() - mOffset;
            }
	    } mData;

		template <typename T>
		void _read(T& object) const {
			auto ptr = reinterpret_cast<byte_t*>(&object);
			mData.read(ptr, sizeof(T));
		}

		template <typename T>
		void _write(const T& object) {
			auto ptr = reinterpret_cast<const byte_t*>(&object);
			mData.write(ptr, sizeof(T));
		}

	public:
		Parcel() = default;

		explicit Parcel(const byte_array_t& bytes) : mData(bytes) {}

		Parcel(const Parcel&) = delete;
		Parcel(Parcel&&) noexcept = delete;
		Parcel&operator=(const Parcel&) = delete;
		Parcel&operator=(Parcel&&) noexcept = delete;

		Parcel&operator=(const byte_array_t& bytes) {
		    mData.mBytes = bytes;
		    mData.mOffset = 0;
            return *this;
		}

#define declare_parcel_rw(TYPE)\
		Parcel&write(const TYPE& value) {\
			_write<TYPE>(value);\
			return *this;\
		}\
		const Parcel&read(TYPE& value) const {\
			_read<TYPE>(value);\
			return *this;\
		}

		declare_parcel_rw(uint8_t)
		declare_parcel_rw(uint16_t)
		declare_parcel_rw(uint32_t)
		declare_parcel_rw(uint64_t)
		declare_parcel_rw(int8_t)
		declare_parcel_rw(int16_t)
		declare_parcel_rw(int32_t)
		declare_parcel_rw(int64_t)
		declare_parcel_rw(float)
		declare_parcel_rw(double)
		declare_parcel_rw(bool)

#undef declare_parcel_rw

		Parcel&write(const Parcelable& p) {
			p.writeToParcel(*this);
			return *this;
		}
		const Parcel&read(Parcelable& p) const {
			p.readFromParcel(*this);
			return *this;
		}


		template <typename CharT>
		Parcel&write(const std::basic_string<CharT>& str) {
		    write(str.size());
		    mData.write(reinterpret_cast<const byte_t*>(str.data()), str.size());
            return *this;
		}
		template <typename CharT>
		const Parcel&read(std::basic_string<CharT>& str) const {
		    size_t size = 0;
		    read(size);
		    str.resize(size, 0);
		    mData.read(reinterpret_cast<byte_t*>(str.data()), size);
            return *this;
		}


		template <typename T>
		Parcel&operator<<(const T& value) {
			return write(value);
		}
		template <typename T>
		const Parcel&operator>>(T& value) const {
			return read(value);
		}


		[[nodiscard]] byte_array_t data() const {
			return mData.mBytes;
		}

		[[nodiscard]] byte_array_t data(size_t startOffset, size_t endOffset) const {
			byte_array_t result;
			auto length = endOffset - startOffset;
			result.resize(length);
			mData.read(result.data(), length);
			return result;
		}

		size_t &getOffset() { return mData.mOffset; }

		const size_t &getOffset() const { return mData.mOffset; }

		size_t getSize() const { return mData.mBytes.size(); }

		bool eof() const {
            return mData.available() == 0;
		}
	};


#define declare_parcel_writer(TYPE, NAME, ACTION)\
	inline Parcel&operator<<(Parcel& out, const TYPE& NAME) {\
        ACTION;\
        return out;\
	}

#define declare_parcel_reader(TYPE, NAME, ACTION)\
	inline const Parcel&operator>>(const Parcel& in, TYPE& NAME) {\
	    ACTION;\
        return in;\
	}

#define declare_typed_parcel_writer(TYPE, SUBTYPE, NAME, ACTION)\
	template <typename SUBTYPE>\
	inline Parcel&operator<<(Parcel& out, const TYPE<SUBTYPE>& NAME) {\
        ACTION;\
        return out;\
	}

#define declare_typed_parcel_reader(TYPE, SUBTYPE, NAME, ACTION)\
	template <typename SUBTYPE>\
	inline const Parcel&operator>>(const Parcel& in, TYPE<SUBTYPE>& NAME) {\
	    ACTION;\
        return in;\
	}


	declare_typed_parcel_writer(std::vector, T, v, {
        out << v.size();
        for (const auto& it : v) {
            out << it;
        }
	})
	declare_typed_parcel_reader(std::vector, T, v, {
        size_t size = 0;
        in >> size;
        v.resize(size);
        for (auto& it : v) {
            in >> it;
        }
	})


	declare_typed_parcel_writer(std::list, T, l, {
	    out << l.size();
	    for (const auto& it : l) {
	        out << it;
	    }
	})
	declare_typed_parcel_reader(std::list, T, l, {
	    size_t size = 0;
	    in >> size;
	    l.resize(size);
	    for (auto& it : l) {
	        in >> it;
	    }
	})


	declare_typed_parcel_writer(std::set, T, s, {
	    out << s.size();
	    for (const auto& it : s) {
	        out << it;
	    }
	})
	declare_typed_parcel_reader(std::set, T, s, {
	    size_t size = 0;
	    in >> size;
        T value;
        while (size--) {
	        in >> value;
	        s.insert(value);
	    }
	})

	declare_typed_parcel_writer(std::unordered_set, T, s, {
	    out << s.size();
	    for (const auto& it : s) {
	        out << it;
	    }
	})
	declare_typed_parcel_reader(std::unordered_set, T, s, {
	    size_t size = 0;
	    in >> size;
        T value;
        while (size--) {
	        in >> value;
	        s.insert(value);
	    }
	})


	template <typename KeyT, typename ValueT>
	inline Parcel&operator<<(Parcel& out, const std::pair<KeyT, ValueT>& pair) {
        return out << pair.first << pair.second;
	}
    template <typename KeyT, typename ValueT>
    inline const Parcel&operator>>(const Parcel& in, std::pair<KeyT, ValueT>& pair) {
        return in >> pair.first >> pair.second;
	}

    template <typename KeyT, typename ValueT, typename _Compare, typename _Alloc>
    inline Parcel&operator<<(Parcel& out, const std::map<KeyT, ValueT, _Compare, _Alloc>& map) {
	    out << map.size();
	    for (const auto& it : map) {
	        out << it;
	    }
	    return out;
	}
    template <typename KeyT, typename ValueT, typename _Compare, typename _Alloc>
    inline const Parcel&operator>>(const Parcel& in, std::map<KeyT, ValueT, _Compare, _Alloc>& map) {
	    size_t size = 0;
	    in >> size;
	    std::pair<KeyT, ValueT> pair;
	    while (size--) {
	        in >> pair;
	        map.insert(pair);
	    }
	    return in;
	}

    template <typename KeyT, typename ValueT, typename _Hash, typename _Pred, typename _Alloc>
    inline Parcel&operator<<(Parcel& out, const std::unordered_map<KeyT, ValueT, _Hash, _Pred, _Alloc>& map) {
	    out << map.size();
	    for (const auto& it : map) {
	        out << it;
	    }
	    return out;
	}
    template <typename KeyT, typename ValueT, typename _Hash, typename _Pred, typename _Alloc>
    inline const Parcel&operator>>(const Parcel& in, std::unordered_map<KeyT, ValueT, _Hash, _Pred, _Alloc>& map) {
	    size_t size = 0;
	    in >> size;
	    std::pair<KeyT, ValueT> pair;
	    while (size--) {
	        in >> pair;
	        map.insert(pair);
	    }
	    return in;
	}


	template <typename T, size_t N>
	inline Parcel&operator<<(Parcel& out, const std::array<T, N>& array) {
	    for (const auto& it : array) {
	        out << it;
	    }
	    return out;
	}
	template <typename T, size_t N>
	inline const Parcel&operator>>(const Parcel& in, std::array<T, N>& array) {
	    for (auto& it : array) {
	        in >> it;
	    }
	    return in;
	}


	declare_parcel_writer(Parcel, p, {
	    out << p.data();
	})
	declare_parcel_reader(Parcel, p, {
	    byte_array_t data;
	    in >> data;
	    p = data;
	})

}

#endif // __egor9814__parcel__parcel_hpp__
