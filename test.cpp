#include <parcel/parcel.hpp>
#include <iostream>

using parcel::Parcel;
using namespace std;

int main() {
	Parcel p1;
	std::set<int> set = { 1, 2, 3, 4, -1, -5 };
	std::list<int> l = { 1, 2, 3, 4, -1, -5 };
	size_t s1{508131678568761}, s2{56};
	p1 << true << 1 << std::string("awesome") << s1 << s2 << set << l << false;
	auto raw = p1.data();
	
	Parcel p2(raw);
	bool b1, b2;
	int i;
	std::string s;
	set.clear();
	l.clear();
	s1 = s2 = 0;
	p2 >> b1 >> i >> s >> s1 >> s2 >> set >> l >> b2;

	cout << "bool 1: " << boolalpha << b1 << endl;
	cout << "   int: " << i << endl;
	cout << "string: " << s << endl;
	cout << "size 1: " << s1 << endl;
	cout << "size 2: " << s2 << endl;
	cout << "   set: { ";
	for (auto it = set.begin(); it != set.end(); ) {
	    cout << *it;
	    if (++it == set.end()) {
	        cout << " }" << endl;
	    } else {
	        cout << ", ";
	    }
	}
	cout << "  list: { ";
	for (auto it = l.begin(); it != l.end(); ) {
	    cout << *it;
	    if (++it == l.end()) {
	        cout << " }" << endl;
	    } else {
	        cout << ", ";
	    }
	}
	cout << "bool 2: " << b2 << endl;
	return 0;
}

