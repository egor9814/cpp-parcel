#!/usr/bin/env bash
if [ ! -d .build ]; then
    mkdir .build
fi
if [ -f 'test' ]; then
	rm -f 'test'
fi

cd .build
cmake .. && make && cp parcel_test ../test
cd ..

